# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 11:47:22 2023

@author: 8288
"""
import os,time
import fitz
from file_type import pdf_to_png
from file_type import tiff_to_png
from functools import wraps
def count_time(func):
    def wrapper(*args, **kwargs):
        time_start = time.time()
        result = func(*args, **kwargs)
        time_end = time.time()
        func_time = time_end - time_start
        print(f'Took {func_time} seconds')
        return result
    return wrapper
@count_time
def file_type_(Inputfile, file_path_input, user_account,file_path):
    root, extension = os.path.splitext(Inputfile)
    Inputfile=os.path.join(file_path_input,Inputfile)
    #判斷png folder有沒有文件，如果有則刪除
    pngfile_list=os.listdir(f'.\{user_account}\{file_path}\png')
    if pngfile_list == []:
        pass
    else:
        for pngfile in pngfile_list:
            os.remove(os.path.join(f'.\{user_account}\{file_path}\png',pngfile))
    #轉檔
    if (extension =='.pdf'or extension =='.PDF'):
        result=pdf_to_png(Inputfile, user_account,file_path)
        
    elif (extension=='.tif'or extension =='.tiff'):
        result=tiff_to_png(Inputfile, user_account,file_path)

    # elif (extension=='.png'or extension =='.PNG'):
    #     return 
    else:
        print(f'格式錯誤，此檔案為{extension}，請匯入pdf檔或是tiff檔')
        result={404:f'格式錯誤，此檔案為{extension}，請匯入pdf檔或是tiff檔'}
        #input('Press Enter to continue...')
    return result

# pdf_path = "./png_output/page_1.png"
# output_folder = "./png_output"
# png_list = file_type(pdf_path, output_folder)
if __name__=='__main__':
    
    file_path=r'./input'
    Inputfile=os.listdir(file_path)[0]
    file_type_(Inputfile,file_path)
    #os.remove(os.path.join(file_path,Inputfile))
    