# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 11:44:55 2023

@author: 8288
"""
import cv2
import numpy as np
from processing import make_gray_pixels_black
def Process(image_name,user_account,file_path):
    path=f'./{user_account}/{file_path}/png_chosen/{image_name}'
    image=cv2.imdecode(np.fromfile(path, dtype=np.uint8), -1)
    # cv2.imwrite(f'o_.png',image)
    if image is None or image.all() == None:
        print("404,Wrong image.")
        #input("Press Enter to continue...")
        return None
    h_img, w_img,__ = image.shape
    if h_img>3600 and w_img>2500:
        image = cv2.resize(image, (1654,2340 ), interpolation=cv2.INTER_AREA)
    gray=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)#灰階   
    gray=make_gray_pixels_black(gray)
    blur=cv2.GaussianBlur(gray,(5,5),0)#高斯濾波
    thresh=cv2.threshold(blur,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)[1]#反白
    
    kernal=cv2.getStructuringElement(cv2.MORPH_RECT,(10,4))
    dilate=cv2.dilate(thresh,kernal,iterations=9)#膨脹
    cnts=cv2.findContours(dilate,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)#框選
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    cnts=sorted(cnts,key=lambda x: cv2.boundingRect(x)[0])
    for c in cnts:
        x,y,w,h = cv2.boundingRect(c)
        
        if w>600 and h>700 and w<1800 and h<1800:
            # print(x,y,w,h)
            if w/h<0.97 and w/h>0.6:
                rio  = image[y:y+h,x:x+w]
                return rio