# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 11:46:49 2023

@author: 8288
"""
import pytesseract
def make_gray_pixels_black(image,threshold=105):
    width, height = image.shape
    frame=image.copy()
    frame[frame < threshold] = 10
    frame[frame > 200] = 255
    return frame
def find_best_threshold(image):
    
    min_threshold = 100
    max_threshold = 200

    best_threshold = min_threshold
    best_ocr_result_length = 0

    for threshold in range(min_threshold, max_threshold ,20):
        rio = make_gray_pixels_black_rio(image, threshold)
        ocr_result1 = pytesseract.image_to_string(rio, lang='chi_tra+eng')
        ocr_result_length = len(ocr_result1)

        if ocr_result_length > best_ocr_result_length:
            best_threshold = threshold
            best_ocr_result_length = ocr_result_length

    return best_threshold
def make_gray_pixels_black_rio(image,threshold):
    
    frame=image.copy()
    frame[frame < threshold] = 20
    frame[frame > threshold] = 255
    return frame
def make_gray_pixels_black_check(image,threshold1,threshold2):
    
    frame=image.copy()
    frame[frame < threshold1] = 0
    frame[frame >threshold2] = 255
    return frame