# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 13:07:05 2023

@author: 8288
"""
import cv2

def process_rio(image):

    if len(image.shape) == 2 or (len(image.shape) == 3 and image.shape[2] == 1):
        gray=image
    else:
        gray=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    # image=make_gray_pixels_black_rio(gray)
    # blur=cv2.GaussianBlur(image,(3,3),0)
    # cv2.imwrite(r'rio.png',image)
    # cv2.imwrite(r'gray.png',gray)
    # cv2.namedWindow('Image', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('Image', 700, 950)
    # cv2.imshow('Image', gray)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    return gray