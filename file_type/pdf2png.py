# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 11:26:55 2023

@author: 8288
"""

import fitz  # PyMuPDF
import os

def pdf_to_png(pdf_path, user_account,file_path):
    output_folder=f'./{user_account}/{file_path}/png'
    os.makedirs(output_folder, exist_ok=True)
    try:
        pdf_document = fitz.open(pdf_path)
    
        png_list = []
        
        for page_number in range(pdf_document.page_count):
            dpi=150
            page = pdf_document[page_number]
            image_list = page.get_pixmap(matrix=fitz.Matrix(dpi/72, dpi/72))
            png_filename = os.path.join(output_folder, f"frame_{page_number}.png")
            png_list.append(f"frame_{page_number}.png")
            image_list.save(png_filename)
        pdf_document.close()
        message="change file to png"
        return {200:message }
    except Exception as e:
        print(f"無法開啟{pdf_path}檔案，檔案格式錯誤或已損毀:{e}")
        message=f"無法開啟{pdf_path}檔案，檔案格式錯誤或已損毀:{e}"
        #input('Press Enter to continue...')
        return {404:message }    

# Example usage
#pdf_path = "input/20200325141642.pdf"
#png_list = pdf_to_png(pdf_path)