import os
from PIL import Image
def tiff_to_png(tiff_path,user_account,file_path):
    try:
        tiff_image = Image.open(tiff_path)
        output_folder=f'./{user_account}/{file_path}/png'
        os.makedirs(output_folder,exist_ok=True)
        tiff_list=[]
        for i in range(tiff_image.n_frames):
            tiff_image.seek(i)
            frame = tiff_image.copy()
            output_filename = os.path.join(output_folder, f"frame_{i}.png")
            tiff_list.append(f"frame_{i}.png")
            frame.save(output_filename, format="PNG")
        print(f"讀取{tiff_path}檔案")
        tiff_image.close()
        message="change file to png"
        return {200:message }
    except Exception as e:
        print(f"無法開啟{tiff_path}檔案，檔案格式錯誤或已損毀:{e}")
        message=f"無法開啟{tiff_path}檔案，檔案格式錯誤或已損毀:{e}"
        #input('Press Enter to continue...')
        return {404:message }