# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 11:56:54 2023

@author: 8288
"""
import pytesseract 
import cv2 #opencv-python
from processing import process_rio,make_gray_pixels_black_rio,find_best_threshold
def OCR(image):
    if image is not None:
        rio= process_rio(image)
        ocr_result1=pytesseract.image_to_string(rio, lang='chi_tra+eng')
        #print(ocr_result1)
        if len(ocr_result1)<570:

            gray=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
            blur=cv2.GaussianBlur(gray,(7,5),0)
            ocr_result =  pytesseract.image_to_boxes(blur, lang='chi_tra+eng')

            ocr_result1 =  pytesseract.image_to_string(blur, lang='chi_tra+eng')
            if  len(ocr_result1)<570:
                best_threshold = find_best_threshold(image)
                rio = make_gray_pixels_black_rio(image, best_threshold)
                blur=cv2.GaussianBlur(rio,(3,3),0)
                ocr_result =  pytesseract.image_to_boxes(blur, lang='chi_tra+eng')
                

        else:
            ocr_result =  pytesseract.image_to_boxes(rio, lang='chi_tra+eng')
        return ocr_result
    else:
        return None
def OCR2(rio):
    rio= process_rio(rio)
    ocr_result =  pytesseract.image_to_boxes(rio, lang='chi_tra+eng')
    return ocr_result,rio