# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 09:34:41 2023

@author: 8288
"""

from  processing import Process#抓出讀取範圍
from ocr import OCR
from detect import detect_err,check_data
import os,time
from print_and_show import print__and_show
from dict2json import dict2json,dict2json_error                     
#算時間
def count_time(func):
    def wrapper(*args, **kwargs):
        time_start = time.time()
        result = func(*args, **kwargs)
        time_end = time.time()
        func_time = time_end - time_start
        print(f'Took {func_time} seconds')
        return result
    return wrapper
def loading(temp, total,num):
    #temp目前進度 num一張完成進度
    temp=temp+num
    print('\r' + '[Progress]:[%s%s]%.2f%%;' % (
        '█' * int(temp * 20 / total), ' ' * (20 - int(temp * 20 / total)),
        float(temp / total * 100)), end='', flush=True)
@count_time                     
def app_run(image_list,user_account,file_path):
    Inputfile_path=os.path.join('.',user_account, file_path, "png_chosen")
    NoneType=[]
    #print(1)
    #確認資料夾有東西，沒有的話json 404
    if image_list is None or image_list==[] :
        dict2json_error(None,404,user_account,file_path)
    else:
        #一張一張掃描
        total=len(image_list)
        temp=0
        for image_name in image_list:
            loading(temp, total,0)
            ocr_result,header_,data_=None,{},{}
            #圖片前處理
            rio=Process(image_name,user_account,file_path)
            loading(temp, total,0.1)
            ocr_result=OCR(rio)#OCR辨識
            loading(temp, total,0.2)
            if ocr_result is not None:

                header,header_err,data,data_err,NoneType_,rio,count_row=detect_err(rio,ocr_result)
                loading(temp, total,0.5)
                if NoneType_:
                    header_,data_,header_err_,data_err_,rio=check_data(header_err,data_err,rio,header,data,count_row)
                    loading(temp, total,0.6)
                    if header_ is None or data_ is None:
                        header_,data_=None,None
                        dict2json_error(image_name,"detected but no result",user_account,file_path)
                        #print("無法辨識，請確認檔案格式及清晰度")
                        #input("Press Enter to continue...")
                        os.remove(os.path.join(Inputfile_path,image_name))
                        loading(temp, total,0.8)
                else:
                    header_,data_,header_err_,data_err_,rio=check_data(header_err,data_err,rio,header,data,count_row)
                    loading(temp, total,0.8)
                if header_ == {} or data_ == {}:
                    dict2json_error(image_name,"detected but no result",user_account,file_path)
                    #print("無法辨識，請確認檔案格式及清晰度")
                    #input("Press Enter to continue...")
                    os.remove(os.path.join(Inputfile_path,image_name))
                    loading(temp, total,1)
                else:
                    dict2json(image_name,header_,data_,user_account,file_path)
                    os.remove(os.path.join(Inputfile_path,image_name))
                    loading(temp, total,1)
            else:
                dict2json_error(image_name,"detected but no result",user_account,file_path)
                #print("無法辨識，請確認檔案格式及清晰度")
                #input("Press Enter to continue...")
                os.remove(os.path.join(Inputfile_path,image_name))
                loading(temp, total,1)
            temp=temp+1
            


def print__and_show(header,data,rio):
#image_list,user_account,file_path=image_list,'account','file'
    import cv2
    print('\n')
    text=0
    for row_y, row_text in header.items():
        print(row_y, row_text)
        text=text+len(row_y)+len(row_text)
    for row_y, row_text in data.items():
        print(row_y, row_text)
        text=text+len(row_y)+len(row_text)
    print('總字數',text)
    cv2.namedWindow('Image', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Image', 700, 950)
    cv2.imshow('Image', rio)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
def show(rio):
    import cv2
    cv2.namedWindow('Image', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Image', 700, 950)
    cv2.imshow('Image', rio)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def app_run_dev(image_list,user_account,file_path):
    image_list,user_account,file_path=image_list,'99','一二三'
    Inputfile_path=os.path.join('.',user_account, file_path, "png_chosen")
    NoneType=[]
    #確認資料夾有東西，沒有的話json 404
    if image_list is None or image_list==[] :
        #dict2json_error(None,404,user_account,file_path)
        print("無法辨識,無檔案")
    else:
        #一張一張掃描
        total=len(image_list)
        temp=0
        for image_name in image_list:
            loading(temp, total,0)
            ocr_result,header_,data_=None,{},{}
            #圖片前處理
            rio=Process(image_name,user_account,file_path)
            loading(temp, total,0.2)
            ocr_result=OCR(rio)#OCR辨識
            #show(rio)
            
            #OCR結果不為NONE
            if ocr_result is not None:
                #簡單除錯並分成上下兩部分
                loading(temp, total,0.4)
                header,header_err,data,data_err,NoneType_,rio,count_row=detect_err(rio,ocr_result)
                #NoneType_ TRUE->無法辨識
                loading(temp, total,0.5)
                if NoneType_:
                    header_,data_,header_err_,data_err_,rio=check_data(header_err,data_err,rio,header,data,count_row)
                    loading(temp, total,1)
                    if header_ is None or data_ is None:
                        header_,data_=None,None
                        #dict2json_error(image_name,"detected but no result",user_account,file_path)
                        print("無法辨識，請確認檔案格式及清晰度")
                        #input("Press Enter to continue...")
                        #os.remove(os.path.join(Inputfile_path,image_name))
                #NoneType_ FALSE->可以辨識->改變參數
                else:
                    header_,data_,header_err_,data_err_,rio=check_data(header_err,data_err,rio,header,data,count_row)
                    loading(temp, total,1)
                if header_ == {} or data_ == {}:
                    #dict2json_error(image_name,"detected but no result",user_account,file_path)
                    print("無法辨識，請確認檔案格式及清晰度")
                    #input("Press Enter to continue...")
                    #os.remove(os.path.join(Inputfile_path,image_name))
                else:
                    #dict2json(image_name,header_,data_,user_account,file_path)
                    print__and_show(header_,data_,rio)
                    #os.remove(os.path.join(Inputfile_path,image_name))
                    
            #OCR結果為NONE
            else:
                #dict2json_error(image_name,"detected but no result",user_account,file_path)
                print("有辨識，但效果不好")
                #input("Press Enter to continue...")
                #os.remove(os.path.join(Inputfile_path,image_name))
            temp=temp+1
