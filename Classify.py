# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 11:33:12 2023

@author: 8288
"""
import os,shutil,openpyxl,json
from dict2json import  dict2json
# from datetime import datetime
def classify(Inputfile_path,image_name,file_name,header_,data_,delta):
    # now=datetime.now()
    # now=datetime.strftime(now, "%m%d%H%M")
    # workbook = openpyxl.load_workbook(r"D:\users\DeskTop\input(classify)\output.xlsx")
    workbook = openpyxl.load_workbook(f"output.xlsx")
    sheet = workbook.active
    # sheet.append(["檔案名稱", "頁數", "是否為格式", "錯誤數", "總字數", "正確率", "錯誤項目"])
    if header_ == {} or data_=={}:
        data =[file_name,image_name,'否',None,None,None,None,delta]
    else:
        # print(image_name,header_,data_)
        json_data,length=dict2json(image_name,header_,data_)
        
        json_file_name =file_name[0:-4]+ image_name[0:-4]+".json"
        output_folder=r'./json'
        os.makedirs(output_folder, exist_ok=True)
        json_file_=os.path.join(output_folder,json_file_name)
        with open( json_file_, 'a',encoding='utf-8') as json_file:
            json_file.write(json_data)
        
        data_ = json.loads(json_data)
        # 初始化计数器
        false_count = 0
        false_list=''
        for item in data_:
            if item.get("normal") is False:
                false_count += 1
                false_list+=item.get("title")+' '+item.get("value")+'\n'
        rate=100*(length-false_count)/length

        data = [file_name,image_name,'是',false_count,length,round(rate,1),false_list,delta]
    
    sheet.append(data)
    workbook.save(f"output.xlsx")