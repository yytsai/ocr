# -*- coding: utf-8 -*-
"""
Created on Wed Nov  8 11:20:56 2023

@author: 8288
"""
import json,os
def dict2json(image_name,header_,data_,user_account,file_path):
    new_data=[]
    
    length=0
    for  title,text in header_.items():
        length=len(title)+len(text)+length
        bool_=True
        if title!='廢棄物比例因數樣品分析結果紀錄表'and text=='':
            bool_=False
        item = {"title": title,"value": text,"normal": bool_}
        new_data.append(item) 
            
        item = {"title": title,"value": text,"normal": bool_}
    for  title,text in data_.items():
        bool_=True
        length=len(title)+len(text)+length
        if len(text)!=17 and len(text)!=9:
            # print(text,len(text))
            bool_=False
        item = {"title": title,"value": text,"normal": bool_}
        new_data.append(item) 
        
    
    json_data = json.dumps(new_data, indent=4, ensure_ascii=False)
    json_file_name =f"{image_name[0:-4]}_output.json"
    output_folder=f'./{user_account}/{file_path}/json'
    os.makedirs(output_folder, exist_ok=True)
    json_file_=os.path.join(output_folder,json_file_name)
    with open( json_file_, 'w',encoding='utf-8') as json_file:
        json_file.write(json_data)


    return json_data,length
def dict2json_error(image_name,error_type,user_account,file_path):
    if image_name is None:
        new_data=[{"title":404,
                   "value":"找不到圖片"
                   }]
        json_file_name =f"NoImg_output.json"
    else:
        new_data=[{"title":image_name,
                   "value":"無法辨識，請確認檔案格式及清晰度"
                   }]
        json_file_name =f"{image_name[0:-4]}_output.json"
    json_data = json.dumps(new_data, indent=4, ensure_ascii=False)
    output_folder=f'./{user_account}/{file_path}/json'
    os.makedirs(output_folder, exist_ok=True)
    json_file_=os.path.join(output_folder,json_file_name)
    with open( json_file_, 'w',encoding='utf-8') as json_file:
        json_file.write(json_data)
    