# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 10:57:49 2024

@author: 8288
"""
from flask import Flask
from flask.views import MethodView
from flask_restful import Api, Resource
import os
from file_type_ import file_type_
from app_run import app_run

from flask_socketio import SocketIO
app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins='*')
api = Api(app)
#轉檔api
class ChangeFileTypePng(MethodView):
    def get(self, user_account,file_path):
            #每個資料夾input位置
            file_path_input = os.path.join('.', user_account, file_path, 'input')
            #每個input內的檔案
            if os.listdir(file_path_input) is not None:
                input_file = os.listdir(file_path_input)[0]
            

                #產出結果正確200錯誤404
                result = file_type_(input_file, file_path_input, user_account,file_path)
            
                os.remove(os.path.join('.', user_account,file_path, 'input',input_file))
            else:
                result ={'404':'no file'}
            return result
#ocr api
class AppRunOcr(MethodView):
    def get(self,user_account,file_path):
        Inputfile_path=os.path.join('.',user_account, file_path, "png_chosen")
        image_list=[file for file in os.listdir(Inputfile_path) if file.endswith(".png")]
        #print(image_list,file_path)
        app_run(image_list,user_account,file_path)
        return {200:'done' }
    
api.add_resource(ChangeFileTypePng, '/ChangeFileTypePng/<user_account>/<file_path>')
api.add_resource(AppRunOcr, '/AppRunOcr/<user_account>/<file_path>')

from dotenv import load_dotenv
load_dotenv()
serverIP = os.getenv("SERVER_IP")
serverPort = os.getenv("SERVER_port")
if __name__ == '__main__':
    from waitress import serve
    serve(app,host=serverIP, port=serverPort)
