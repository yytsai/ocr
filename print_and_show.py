# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 11:30:29 2023

@author: 8288
"""
import cv2
def print__and_show(header,data,rio):
    for row_y, row_text in header.items():
        print(row_y, row_text)
    for row_y, row_text in data.items():
        print(row_y, row_text)

    cv2.namedWindow('Image with Text Boxes', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Image with Text Boxes', 700, 950)
    cv2.imshow('Image with Text Boxes', rio)
    cv2.waitKey(0)
    cv2.destroyAllWindows()