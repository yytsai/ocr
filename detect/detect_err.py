# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 13:28:51 2023

@author: 8288
"""
def print_(data):
#image_list,user_account,file_path=image_list,'account','file'
    print('\n')
    for row_y, row_text in data.items():
        print(row_y, row_text)





def detect_err(rio,ocr_result):#第一次辨識與旋轉校正
    from  detect import sort_rio
    ocr_text=sort_rio(rio,ocr_result)#OCR文字照一行行排
    from  change_format import remove_and_sort
    row_dic=remove_and_sort(ocr_text)#最下面那行備註刪掉加上刪除多於符號
    count_row=len(row_dic)
    
    from  detect import detect_bq
    header_dict,data_dict=detect_bq(row_dic)#拆成兩部中文資訊(header)與核項目(data)
   # print_(header_dict)
    from  detect import Rotation_check
    header,header_err,data,data_err,NoneType,rio,count_row=Rotation_check(header_dict,data_dict,rio,count_row)#傾斜校正
    # print_(header)
    # print_(data)
    # print(header_err,data_err)
    return header,header_err,data,data_err,NoneType,rio,count_row