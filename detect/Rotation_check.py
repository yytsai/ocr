# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 14:04:40 2023

@author: 8288
"""
from detect.Rotation import Rotation
from change_format import  change_header_format
from change_format import  change_data_format
def Rotation_check(header_dict,data_dict,rio,count_row):
    
    NoneType=False
    #確認資header資料行數，未滿9表示有可能歪斜
    if header_dict is None:
        
        NoneType=True#無法辨識或格式不同
        return None,None,None,None,NoneType,rio,count_row
    elif len(header_dict) < 10:#旋轉校正
        another_sheet_bool=len(header_dict)==8 and any('廢料源代號' in value for value in header_dict.values()) is False and any('廢料源名稱' in value for value in header_dict.values()) is False
        old_sheet_bool=len(header_dict)==9 and any('核種名稱' in value for value in header_dict.values()) is False
        correct_header_err=0
        correct_count_row=0
        if  another_sheet_bool:
            # print(header_dict)
            header_dict_r,data_dict_r,rio,count_row=header_dict,data_dict,rio,count_row
            correct_count_row=2
            header_dict_r[-10]='廢料源代號: DLLRW'
            header_dict_r[-11]='廢料源名稱: 核一處理費棄物'
            
            pass
        elif old_sheet_bool:
            header_dict_r,data_dict_r,rio,count_row=header_dict,data_dict,rio,count_row
            
            pass
        else:
            header_dict_r,data_dict_r,rio,count_row=Rotation(rio)
        if data_dict_r is not None:
            if  len(header_dict_r)==11:
                count_row=count_row-1
            header_dict,data_dict=header_dict_r,data_dict_r
            #print(header_dict)
            header,header_err=change_header_format(header_dict)
           # print(header,header_err)
            data,data_err=change_data_format(data_dict) 
            header_err=header_err+correct_header_err
            count_row=count_row+correct_count_row
            
            return header,header_err,data,data_err,NoneType,rio,count_row
            
        else:
           # print(len(header_dict))
            NoneType=True#無法辨識
            # print(header_dict)
            header,header_err=change_header_format(header_dict)
            return header,header_err,None,None,NoneType,rio,count_row
        
            
    else:
        if  len(header_dict)==11:
            count_row=count_row-1
        header,header_err=change_header_format(header_dict)
        data,data_err=change_data_format(data_dict) 
        #print(data)
       # print(count_row,len(data),len(header))
        if count_row -len(data)!= len(header):
            data_err=data_err+abs(count_row -10-len(data))*17
        return header,header_err,data,data_err,NoneType,rio,count_row