# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 14:05:45 2023

@author: 8288
"""
import cv2
from  detect.detect_BQ import detect_bq
from  detect.sort_rio import sort_rio
from change_format.remove_ import remove_
from ocr import OCR2
def Rotation(rio):
    # cv2.imwrite('nonrmal.png',rio)
    for angle in [-1.0,1.0]:
        image = rio
        height, width = image.shape[:2]
        center = (width // 2, height // 2)
        rotation_matrix = cv2.getRotationMatrix2D(center, angle, 1.0)
        rotated_height, rotated_width = image.shape[:2]
        rotated_image = cv2.warpAffine(image, rotation_matrix, (rotated_width, rotated_height), borderValue=(255, 255, 255))
        ocr_result,rio2=OCR2(rotated_image)
        row_dic={}
        if ocr_result is not None:
            ocr_text=sort_rio(rio2,ocr_result)#OCR文字排序
            for row_y, row_text in reversed(ocr_text.items()):
                row_text=remove_(row_text)#刪除多於符號
                row_dic[row_y]=row_text
            count_row=len(row_dic)
            header_dict,data_dict=detect_bq(row_dic)
            if header_dict is not None and len(header_dict) >= 10:
                # print('轉',angle,'度')
                return header_dict,data_dict,rotated_image,count_row
            else:
                pass
    return None,None,rio,0