# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 13:43:50 2023

@author: 8288
"""
import cv2
from detect.process_sheet import process_sheet
def Process_row(rio):
    try:
        gray=cv2.cvtColor(rio,cv2.COLOR_BGR2GRAY)
    except cv2.error:
        gray=rio
    thresh_r=cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)[1]#反白
    h1, w1 = thresh_r.shape
    kernal_r=cv2.getStructuringElement(cv2.MORPH_RECT,(150,1))
    dilate_r=cv2.dilate(thresh_r,kernal_r,iterations=1)#膨脹
    cv2.waitKey(0)
    cnts=cv2.findContours(dilate_r,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)#框選
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    cnts=sorted(cnts,key=lambda x: cv2.boundingRect(x)[0])
    if len(cnts)>33:
        kernal_r=cv2.getStructuringElement(cv2.MORPH_RECT,(w1,1))
        dilate_r=cv2.dilate(thresh_r,kernal_r,iterations=1)#膨脹

        cnts=cv2.findContours(dilate_r,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)#框選
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    # cv2.namedWindow('1234', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('1234', 700, 950)
    # cv2.imshow("1234", dilate_r)
    # cv2.waitKey(0)
    c_list=[]
    for c in cnts:
        x,y,w,h = cv2.boundingRect(c)
        # print(x,y,w,h)
        y = h1 - y
        if h  > 600:
            c_list=process_sheet(rio,x,y,w,h,c_list)
        elif h<20 or w<90:
            pass
        else:
            c_list.append((x,y,w,h))
    rio1=rio.copy()
    c_list1=[]
    for x, y, w, h in c_list:
        y=h1-y
        if h>45:
            c_list1.append((x,h1-y,w,h//2))
            c_list1.append((x,h1-(y+h//2),w,h//2))
        else:
            c_list1.append((x,h1-y,w,h))

        cv2.rectangle(rio1,(x,y),(x+w,y+h),(0,170,255),1)
    # cv2.namedWindow('1234', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('1234', 700, 950)
    # cv2.imshow("1234", rio1)
    # cv2.waitKey(0)


    return c_list1