# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 13:38:53 2023

@author: 8288
"""
from detect import Process_row
import cv2
def sort_rio(rio,boxes_info):
    # boxes_info=ocr_result
    row_list=Process_row(rio)#每一行的範圍
    # sorted_boxes_info = sorted(boxes_info.splitlines(), key=lambda data: int(data.split(" ")[1])+0.3*int(data.split(" ")[3]))

    sorted_boxes_info = sorted(boxes_info.splitlines(), key=lambda data: int(data.split(" ")[1]) + 5 + 0.3 * int(data.split(" ")[3]) if int(data.split(" ")[3]) - int(data.split(" ")[1])>= 20 else int(data.split(" ")[1]) + 0.3 * int(data.split(" ")[3]))
    
    sorted_ = sorted(row_list, key=lambda data: int(data[1]))
    text_by_row = {}
    h_by_row = {}
    text_x={}
    clone=rio.copy()
    if len(rio.shape)==3:
        
        hr, wr,__ = rio.shape
    else:
        hr, wr = rio.shape
    for x, y, w, h in row_list:
        text_by_row[y] = []
        text_x[y]=[]
        h_by_row[y]  = h    
        

    for box_info in sorted_boxes_info:
        
        data = box_info.split(" ")
        x1, y1, x2, y2 = int(data[1]), int(data[2]), int(data[3]), int(data[4])
        y=(y1+y2)/2
        # cv2.rectangle(clone,(x1,hr-y1),(x2, hr-y2),(0,0,255),1)
        char = data[0]
        for row_y, row_text in text_by_row.items():
            if y <= row_y and y >= (row_y - h_by_row[row_y]):
                text_x[row_y].append(x1)
                if text_x[row_y]==[x1]:
                    pass
                elif char in'廢棄物比例因數樣品分析結果紀錄表廠別機組廢料源代號廢料源名稱樣品編號取樣日期運送列印核種名稱' :
                    pass
                elif abs(text_x[row_y][-2]-x1)>120:
                        row_text.append(': ')
                row_text.append(char)
    
    return text_by_row