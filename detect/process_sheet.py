# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 13:45:00 2023

@author: 8288

處理特殊格式
"""

import cv2
def process_sheet(rio,x,y,w,h,c_list):
    # 裁切區域的 x 與 y 座標（左上角）
    # x,y,w,h=0,382,961,718
    try:
        hr, wr,__ = rio.shape
    except ValueError:
        hr, wr= rio.shape
    y=hr-y
    y = y+40
    h = h-40

    crop_img = rio[y:y+h, x:x+w]
   
    try:
        gray=cv2.cvtColor(crop_img,cv2.COLOR_BGR2GRAY)
        
    except cv2.error:
        gray=crop_img
    edges = cv2.Canny(gray, 30, 150)
    contours, _ = cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)  
    h_max=0
    for contour in contours:
        x0, y0, w0, h0 = cv2.boundingRect(contour)
        # print(x0, y0, w0, h0)
        # cv2.rectangle(rio,( x0,y0),( x0+w0,y0+h0),(0,170,255),2) 
        if h0>h_max:
            x_max,y_max,w_max,h_max=x0, y0, w0, h0
    clone=crop_img.copy()
    clone_r=rio.copy()
    # cv2.rectangle(clone,(x_max,y_max),(x_max+w_max,y_max+h_max),(0,170,255),3)
    # cv2.rectangle(clone_r,(x+50,y-30),(x+w-100,y+5),(0,170,255),2)
    crop_img2 = crop_img[y_max:y_max+h_max, x_max+6:x_max+w_max-12]    

    try:
        gray=cv2.cvtColor(crop_img2,cv2.COLOR_BGR2GRAY)
    except cv2.error:
        gray=crop_img2
    thresh_r=cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)[1]#反白
    if thresh_r is None:
        return c_list
    h1, w1 = thresh_r.shape
    thresh_r[::,0:5]=0
    thresh_r[::,-3:]=0
    thresh_r[:3,::]=0
    thresh_r[-3:,::]=0
    kernal_r=cv2.getStructuringElement(cv2.MORPH_RECT,(w1*1,1))
    dilate_r=cv2.dilate(thresh_r,kernal_r,iterations=1)#膨脹
    # cv2.namedWindow('123', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('123', 700, 950)
    # cv2.imshow("123", thresh_r)
    # cv2.waitKey(0)
    # cv2.namedWindow('1234', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('1234', 700, 950)
    # cv2.imshow("1234", dilate_r)
    # cv2.waitKey(0)
    cnts=cv2.findContours(dilate_r,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)#框選
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    cnts=sorted(cnts,key=lambda x: cv2.boundingRect(x)[0]) 
    c_list.append((x+x_max+50,hr-y-y_max+30,w-150,35))#核種範圍
    for c in cnts:
        
        x_,y_,w_,h_ = cv2.boundingRect(c)
        # print( x_,y_,w_,h_)
        if h_<17:
            continue
        else:
            # print(h_)
            cv2.rectangle(clone_r,(x+x_max+x_,y+y_+y_max),(x+x_+w_,y+y_+h_),(0,170,255),1)

            y_1 = hr-(y+y_+y_max)
            c_list.append((x+x_max+x_,y_1,w_,h_))
    # cv2.namedWindow('1234', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('1234', 700, 950)
    # cv2.imshow("1234",clone_r)
    # cv2.waitKey(0)
    return(c_list)