# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 16:36:42 2023

@author: 8288
"""


def detect_bq(ocr_text):
    split_line = None
    for line_num, line in ocr_text.items():
        line=''.join(line)
        
        if '備註' in line:
            # print(line)
            pass
        elif "核種名稱"in line or '樣品活度'in line or 'Bq'in line or 'Bd/g'in line or '活度'in line:
            split_line = line_num
            break
        # else:
        #     print('找不到"核種名稱"位置')

    if split_line is not None:
        header_dict = {line_num: line for line_num, line in ocr_text.items() if line_num >= split_line}
        data_dict = {line_num: line for line_num, line in ocr_text.items() if line_num < split_line and len(line)>5}
        return header_dict,data_dict
    else:
        return None,None
        

