# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 16:56:17 2023

@author: 8288
"""
import cv2,pytesseract
from processing import make_gray_pixels_black_check
from detect import detect_err
def data_repalce(data_,data_min,data_err_):
    real_typename = ['H-3', 'C-14', 'CR-51', 'Mn-54', 'Fe-55', 'Co-57', 'Co-58', 'Ni-59', 'Fe-59', 'Co-60', 'Ni-63', 'Zn-65', 'Sr-90', 'Nb-94', 'Zn-95', 'Tc-99', 'Cd-109', 'Ag-110m', 'Sn-113', 'Sb-125', 'I-129', 'Cs-134', 'Cs-137', 'Ba-140', 'Ce-141', 'Ce-144', 'U-234', 'U-235', 'Pu-238', 'U-238', 'Pu-239/240', 'Pu-241', 'Am-241', 'Cm-242', 'Am-244', 'Cm-244']
    for data_type in data_min:
        data_min_len_is_correct=len(data_min[data_type])==17 or len(data_min[data_type])==9
        data_type_is_in_data_data_=data_type in data_
        if data_type_is_in_data_data_ is False and data_min_len_is_correct is True :
            data_[data_type]=data_min[data_type]
            data_err_=data_err_-17
        if data_type_is_in_data_data_ is True:
            data_len_is_correct=len(data_[data_type])==17 or len(data_[data_type])==9
            if data_len_is_correct is False and data_min_len_is_correct is True:
                error=abs(len(data_min[data_type])-len(data_[data_type]))
                data_[data_type]=data_min[data_type]
                
                data_err_=data_err_-error
    sorted_data_ = {}

    for key in real_typename:
        if key in data_:
            sorted_data_[key] = data_[key]
    return sorted_data_,data_err_
            
            
    
def check_data(header_err,data_err,rio,header,data,count_row):
    header, header_err = ({}, 50) if header is None else (header, header_err)
    data, data_err = ( {}, 40) if  data is None else ( data, data_err)
    header_min,h_min_error,data_min, d_min_error=header, header_err,data, data_err
    # print(data)
    clone=rio.copy()
    if data_err + header_err > 0 or 'Cm-244' not in data or count_row != len(header) + len(data):
        
        blur_radii = [(3, 3), (0,0),(7, 5), (5, 5), (3, 5),(5,3)]
        
        for blur_radius in blur_radii:
            gray = cv2.cvtColor(rio, cv2.COLOR_BGR2GRAY)
            if blur_radius == (0,0):
                processed_image = gray

            elif blur_radius == (3,5):
                gray1=make_gray_pixels_black_check(gray,100,180)
                processed_image=cv2.GaussianBlur(gray1,blur_radius,0)

            elif blur_radius == (5,3):
                gray1=make_gray_pixels_black_check(clone,100,180)
                processed_image=cv2.GaussianBlur(gray1,blur_radius,0)

            else:
                processed_image = cv2.GaussianBlur(gray, blur_radius, 0)

            ocr_result = pytesseract.image_to_boxes(processed_image, lang='chi_tra+eng')
            header_, header_err_, data_, data_err_, _, rio, count_row_ = detect_err(rio, ocr_result)
            
            # if blur_radius == (3,3):
            #     print(data_, data_err_)
            #print(blur_radius,blur_radii.index(blur_radius) + 1,' 改變參數')  # To print 1, 2, 3, or 4
            
            if header_ is not None and data_ is not None and count_row_ -len(data_)== len(header_) and data_err_ + header_err_ <= 0:
                header_min,data_min,h_min_error,d_min_error=header_,data_,header_err_,data_err_
                break
            elif header_ is None  and data_ is None:
                header_, header_err_, data_, data_err_ = {}, 50, {}, 40
            elif header_ is None:
                header_, header_err_={}, 50
            elif data_ is None:
                data_, data_err_ = {}, 40
            elif count_row_ -len(data_)!= len(header_):
                data_,data_err_=data_repalce(data_,data_min,data_err_)
                #print(data_err_,count_row_,len(data_))
                data_err_=data_err_+abs(count_row_ -10-len(data_))*17
            #print(data_err_,d_min_error)
            if header_err_<h_min_error and len(header_)>=10:
                # print(header_err_,h_min_error)
                header_min=header_
                h_min_error=header_err_
            # print(count_row_,len(data_))
            if data_err_<d_min_error and len(data_)==count_row_-len(header_):
                data_min=data_
                d_min_error=data_err_   
            # print(len(data_),data_)
            # print(header_min,data_min,header_err_)
            
    else:
        return header,data,header_err,data_err,rio
    
    header_,data_,header_err_,data_err_=header_min,data_min,h_min_error,d_min_error

        
    if  'Cm-244' in data_ and 'Cm-244' in data :
        if len(data_['Cm-244'])<len(data['Cm-244']):
            data_['Cm-244']=data['Cm-244']
            if  len(data_['Cm-244'])==17   or len(data_['Cm-244'])==9:
                data_err_=data_err_-1
    elif 'Cm-244' in data_:
        data['Cm-244']=data_['Cm-244']
    elif 'Cm-244' in data:
        data_['Cm-244']=data['Cm-244']
        if  len(data_['Cm-244'])==17   or len(data_['Cm-244'])==9:
            data_err_=data_err_-1


    header,header_err = (header,header_err) if header_err < header_err_ and len(header_)< len(header)  else (header_,header_err_)
    data,data_err = (data,data_err) if data_err < data_err_ and len(data_)< len(data) else (data_,data_err_)
    return header,data,header_err,data_err,rio