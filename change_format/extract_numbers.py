# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 14:32:32 2023

@author: 8288
"""
from change_format.char_filter import char_filter
def extract_numbers(text):
    # text='2.1TE-02+432E-03'
    if "'"in text:
        text=text.replace("'","").replace("%","")
    numbers = []
    current_number = ''
    irregular=False
    if len(text) == 17:
        text=text[:8] + '±' + text[9:]
        if text[5] in '1/4!i':
            text=text[:5] + '+' + text[6:]
        if text[14] in '1/4!i':
            text=text[:14] + '+' + text[15:]
        if text[4]=='8'and text[5]=='+':
            text=text[:4] + 'E' + text[5:]
        if text[4]=='8'and text[5] in '+-':
            text=text[:4] + 'E' + text[5:]
        if text[13]=='8'and text[14] in '+-':
            text=text[:13] + 'E' + text[14:]
        if text[-3]=='T':
            text=text[:-3] + '+' + text[-2:]
        for char in text:
            char=char_filter(char)
            if  char in [ '-', '+', 'E']:
                if current_number != '':
                    numbers.append(current_number)
                numbers.append(char)
                current_number = ''
            else:
                current_number += char
        text=current_number#最後一個
    elif len(text) == 9:
        if text[-3]=='7':
            text=text[:-3] + '+' + text[-2:]
        if text[5]=='8' and ("E" in text) is False:
            text=text[:5] + 'E' + text[6:]
        for char in text:
            char=char_filter(char)
            if  char in [ '-', '+', 'E','<']:
               if current_number != '':
                   numbers.append(current_number)
               numbers.append(char)
               current_number = ''
            else:
               current_number += char
        text=current_number#最後一個
    elif len(text) == 8:
        if '<' not  in text:
            numbers.append('<')
        else:
            irregular=True  
    elif len(text) == 16:
        if text[4]=='E'and text[12]=='E'and text[5] in '+-':
            if text[8]=='+':
                text=text[:8]+'±'+text[9:]
                if text[1]!='.':
                    text=text[:2]+'.'+text[2:]
                elif text[10]!='.':
                    text=text[:10]+'.'+text[10:]
                else:
                    irregular=True
            else:
    
                text=text[:8] + '±' + text[8:]
        else :
            irregular=True

            
    else:
        
        if len(text) == 18 and text[8]=='+':
            if  text[9]=='4' and text[11]=='.':
                text=text[:8] + '±' + text[10:]
            else:
                irregular=True
                text=text[:8] + '±' + text[9:]
        elif len(text) == 18 and text[1]=='.'and text[4]=='8'and text[5]=='E':
            text=text[:4] +  text[5:]
            if text[8]=='4' and text[10]=='.'and text[1]=='.':
                text=text[:8] + '±' + text[9:]
            else:
                irregular=True
        elif len(text) == 10 and 'E£' in text:
            text=text.replace('E£','E')
        else:
            irregular=True
            
    for char in text:
        char=char_filter(char)
        numbers.append(char)
    numbers=''.join(numbers)
    if '0.'in numbers:
        numbers=numbers.replace('0.','6.')
    # if irregular:
        # print(numbers)
    #print(numbers,irregular)
    return numbers,irregular