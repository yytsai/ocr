# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 14:19:10 2023

@author: 8288
"""
from change_format import accurate
from change_format.row_to_real import row_to_real as row_to_real
def change_header_format(header_dict):
    #print(header_dict)
    header={}
    
    for row, row_text in header_dict.items():
        real_headername=['廢棄物比例因數樣品分析結果紀錄表','廠別','機組','廢料源代號','廢料源名稱','樣品編號','取樣日期','運送日期','列印日期','核種名稱B','核種名稱樣品活度']
        parts = row_text.split(":")
        # print(parts)
        if len(parts) == 2:
            type_=accurate(real_headername,parts[0])
            
            if type_== ' ':
                header[parts[0]]=parts[1]
            elif type_== '核種名稱B'or type_=='核種名稱樣品活度':
                header['核種名稱']=parts[1]
            else:
                header[type_]=parts[1]
        elif len(parts) > 2:
            parts = [part for part in parts if part.strip()]
            type_=accurate(real_headername,parts[0])
            if type_ not in real_headername:
                type_=accurate(real_headername,row_text)
            if len(parts) == 2:
                header[type_]=parts[1]
            else:
                join=''.join(parts[1:])
                header[type_]=join.replace(' ','')
        else:
            # print(parts)
            type_=accurate(real_headername,row_text)
            if type_ == '廢棄物比例因數樣品分析結果紀錄表':
                header[type_]=""
            elif type_ == '核種名稱B' or type_=='核種名稱樣品活度' :
                header['核種名稱']=row_text
            elif ":" not in row_text:
                if type_ in  header and type_ in real_headername:
                    # print(parts)
                    real_headername.remove(type_)
                    type_=accurate(real_headername,row_text)
                    header[type_]=row_text[len(type_):]
                else:
                    header[type_]=row_text[len(type_):]
    real_header={}
    real_headername1=['廢棄物比例因數樣品分析結果紀錄表','廠別','機組','廢料源代號','廢料源名稱','樣品編號','取樣日期','運送日期','列印日期','核種名稱']
    count=0
    for output_header in real_headername1:
        
        try:
            raw=header[output_header]
            
            # print(raw)
            if raw == '':
                real_header[output_header]=''
                if '日期' in output_header:
                    count+=10
                elif output_header in ['廢料源代號','廢料源名稱','樣品編號']:
                    count+=5
                    
                elif output_header =='廢棄物比例因數樣品分析結果紀錄表':
                    pass
                else:    
                    count+=3
            else:
                if raw == '核廠':
                    count+=1
                real_data=row_to_real(output_header,raw)
                if output_header!='廢棄物比例因數樣品分析結果紀錄表' and real_data=='':
                    count+=3
                if '日期' in output_header and len(raw)<6:
                    
                    count=count+(6-len(raw))
                real_header[output_header]=real_data
        except KeyError:
            real_header[output_header]=''
            if '日期' in output_header:
                count+=10
            elif output_header in ['廢料源代號','廢料源名稱','樣品編號']:
                count+=5
            elif output_header =='廢棄物比例因數樣品分析結果紀錄表':
                pass
            else:    
                count+=3
    if real_header['廢料源代號']=='SR(CVCS)':
        real_header['廢料源名稱']='廢樹脂(CVCS)'
    return real_header,count