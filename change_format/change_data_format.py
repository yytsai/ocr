# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 14:30:55 2023

@author: 8288
"""
from collections import OrderedDict
from change_format import accurate
from change_format.extract_numbers import extract_numbers
def change_data_format(data_dict):
    data={}
    sorted_data = OrderedDict(sorted(data_dict.items(), reverse=True))
    real_typename=['H-3',
                   'C-14',
                   'CR-51',
                   'Mn-54',
                   'Fe-55',
                   'Co-57',
                   'Co-58',
                   'Ni-59',
                   'Fe-59',
                   'Co-60',
                   'Ni-63',
                   'Zn-65',
                   'Sr-90',
                   'Nb-94',
                   'Zn-95',
                   'Tc-99',
                   'Cd-109',
                   'Ag-110m',
                   'Sn-113',
                   'Sb-125',
                   'I-129',
                   'Cs-134',
                   'Cs-137',
                   'Ba-140',
                   'Ce-141',
                   'Ce-144',
                   'U-234',
                   'U-235',
                   'Pu-238',
                   'U-238',
                   'Pu-239/240',
                   'Am-241',
                   'Pu-241',
                   'Cm-242',
                   'Am-244',
                   'Cm-244']
    ire__count=0
    for row, row_text in sorted_data.items():
        parts = row_text.split(": ")
        if len(parts)== 2:
            if  row>500 and 'I9'in parts[0] :
                type_='H-3'
            type_= accurate(real_typename,parts[0])
            if "日" in type_ :
                type_='H-3'
            # print(parts[1])
            data1,irregular= extract_numbers(parts[1])
            if irregular:
                # print(data1)
                ire__count+=1

            data[type_]=data1
        elif len(parts) > 2:
            # print(parts)1
            parts = [part for part in parts if part.strip()]
            type_=accurate(real_typename,parts[0])
            if len(parts) == 2:
                data1,irregular= extract_numbers(parts[1])
                if irregular:
                    ire__count+=1

                data[type_]=data1
            else:
                type_=None
                for part in parts:
                    if len(part)>=3 and len(part)<=8:
                        type_= accurate(real_typename,part)
                    elif len(part)>8 and type_ is not None:
                        data1,irregular= extract_numbers(part)
                        data[type_]=data1
                    else:
                        ire__count+=1
        #print(type_,data[type_],ire__count)
    
    return data,ire__count