# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 14:21:21 2023

@author: 8288
"""
from change_format import accurate
def row_to_real(output_header,raw):
    # print(output_header)
    if output_header=='廠別':
        checklist=['核三廠','核二廠','核一廠']
        real_data=accurate(checklist,raw)
        if real_data not in checklist:
            real_data='核廠'
    elif output_header=='機組':
        if raw == '(' or raw == '(':
            raw='0'
        checklist=['0','1','2','5','6']
        real_data=accurate(checklist,str(raw))
        if real_data not in checklist:
            real_data=''
            
    elif output_header=='廢料源代號':
        
        if raw == '(A' or raw == '(B':
            raw=raw.replace('(','C')
        elif raw in '子聊孔隕了元堵陸陡了抑了即陛和了':
            raw= 'EB'
        elif raw == '日':
            raw='B'
        elif raw=='(':
            raw='C'
        checklist=['CVCS','FILTER','SR(R/W)','EB','SLUDGE(SL)','CA','CB','B','C','FR','DAW-C','DAW-NC','C/U','SR','WST','FC','CWT','WST','中子偵測元件','BB','EBF','F','HEB','HEBF','SR','FD','FF','MIX','SELF','DLLRW','SR(CVCS)']
        real_data=accurate(checklist,raw)
    elif output_header=='廢料源名稱':
        checklist=['CVCS樹脂','過濾器 smear paper','廢樹脂R/W','濃縮廢液','高濃縮廢液','沉澱殘渣','可燃乾性廢料','CB smear paper',
                   '廢樹脂','不可燃乾廢料(smear paper)','廢液淨化殘渣BSL(Sludge)','廢液淨化殘渣R/W','爐水淨化殘渣BCR','爐水淨化殘渣R/W','不可燃乾性廢料(SMEAR)','可燃乾廢料','不可燃乾廢料','爐水淨化殘渣RWCU','核一除役廢棄物']
        real_data=accurate(checklist,raw)
    elif output_header=='核種名稱':
        checklist=['樣品活度(Bq/g)','樣品活度(Bq/EA)',]
        real_data=accurate(checklist,raw)
    elif output_header=='樣品編號':
        # print(raw)
        # raw='9313095'
        raw=raw.replace('+', '').replace('>', '').replace('$', '8').replace(',', '').replace('v','').replace('本','')
        if raw == '':
            real_data=''
        elif len(raw)==7 and raw[3]=='1':
            if raw[0]=='9'and raw[2]=='T':#971T103->97T103
                real_data=raw[0:2]+'T'+raw[4:]
            elif raw[4]=='9':#1031901->103T301
                real_data=raw[0:3]+'T'+'3'+raw[5:]
            else:
                real_data=raw[0:3]+'T'+raw[4:]
        elif len(raw)==7 and raw[4]=='9':
            real_data=raw[0:3]+'T'+'3'+raw[5:]
        elif len(raw)==7 and raw[0]=='9'and raw[2]=='1'and 'T'not in raw:
            real_data=raw[0:2]+'T'+raw[3:]
        elif len(raw)==8 and raw[3]=='T'and raw[4]=='1':
            real_data=raw[0:3]+'T'+raw[5:]
        elif len(raw)==8 and '38'in raw:
            real_data=raw.replace('38','3')
        elif len(raw)==6 and raw[0]=='9'and raw[2]=='1':
            real_data=raw[0:2]+'T'+raw[3:]
        elif len(raw)==9 and raw[-2]=='-'and raw[3]=='1':
            real_data=raw[0:3]+'T'+raw[4:]
        else:
            real_data=raw
    elif '日期' in output_header:
        raw=raw.replace("—","").replace("”","").replace(">","").replace("=","").replace("3021","2021")
        real_data=raw
        if '+'in raw:
            real_data=raw.replace('+','')
        if raw[-2:]=='91':
            real_data=raw[:-2]+'31'          
    else:
        real_data=raw
    
    return real_data