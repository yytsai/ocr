# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 13:49:07 2023

@author: 8288
"""
from change_format  import remove_
def remove_and_sort(ocr_text):
    row_dic={}
    for row_y, row_text in reversed(ocr_text.items()):
        row_text=remove_(row_text)#刪除多於符號
        if len(row_text)<3 or '註:' in row_text or '備註:' in row_text:#最下面那行備註刪掉
            pass
        else:
            row_dic[row_y]=row_text
    return row_dic