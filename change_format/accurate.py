# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 14:20:44 2023

@author: 8288
"""
import difflib
def accurate(real_list,detect_name):
    if detect_name in real_list:
        return detect_name
    else:
        accurate=0
        max_type=' '
        for i in real_list:
            try:
                matcher = difflib.SequenceMatcher(None, i, detect_name)
                similarity = matcher.ratio()
            except TypeError:
                print( i, detect_name,'出錯了')
                break
            if similarity>accurate:
                accurate=similarity
                max_type=i
        if accurate==0:
            return detect_name

        return max_type